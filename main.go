package main

import (
	"encoding/json"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"io/ioutil"
	"net/http"
)

// utility function to connect to redis
func connect() (redis.Conn, error) {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		panic(err)
	}
	return c, err
}

// small pool of redis connections.
// One connection is held by goroutine to process requests
// Other connections are used ad-hoc by the status endopint.
var pool = redis.NewPool(connect, 3)

// A chan for pending requests.
// Pending requests are represented by a seat range, which initially
// contains only section and desired size.
// If the request is filled, the SeatRange will be puplated
// with specific seat number and Success will be true.
var pending = make(chan *SeatRange)

// load lua script onto redis server
var findSeatRange = loadScript(1, "find_seat_range.lua")

// utility function to load lua script
func loadScript(args int, name string) *redis.Script {
	script, err := ioutil.ReadFile("redis_scripts/" + name)
	if err != nil {
		panic(err)
	}
	result := redis.NewScript(args, string(script))
	return result
}

// populate DB with stadium layout specified in the assignment
func seed() {
	db := pool.Get()
	defer db.Close()
	seed := loadScript(0, "seed.lua")
	seed.Do(db)
}

// A friendly hello endpoint
func handleHello(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello\n"))
}

// The /reserve endpoint.
// Currently this just generates a random seciton and party size request
// for testing.  In reality these would be query parameters.
// Creates a SeatRange for the request (section and size)
// and places on the pending channel to be processed by goroutine.
// This endpoint holds the HTTP connection open until it recieves
// on the SeatRange `done` channel, at which point it responds to the client
// with the results (success or failure).
func handleReservation(w http.ResponseWriter, r *http.Request) {

	section := fmt.Sprintf("Section%d", randomSection())
	size := randomPartySize()

	sr := &SeatRange{Section: section, Size: size, done: make(chan int, 1)}
	pending <- sr
	<-sr.done

	encoder := json.NewEncoder(w)
	encoder.Encode(sr)
}

// utility endoint to reset the database for testing
func handleSeed(w http.ResponseWriter, r *http.Request) {
	seed()
	w.Write([]byte("seeded\n"))
}

// struct for the /stats endpoint to get # free / # reserved seats.
type stats struct {
	Free int64 `redis:"free"`
	Hold int64 `redis:"hold"`
}

// stats endpoint
func handleStats(w http.ResponseWriter, r *http.Request) {
	db := pool.Get()
	defer db.Close()
	var s stats
	reply, err := redis.Values(db.Do("HGETALL", "count"))
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
	if err := redis.ScanStruct(reply, &s); err != nil {
		http.Error(w, err.Error(), 500)
	} else {
		encoder := json.NewEncoder(w)
		encoder.Encode(s)
	}
}

// This function runs in a separate goroutine to process requests on the pending
// channel.  It calls the lua script on the redis server to find a matching
// seat range if available.
func processPending() {
	db := pool.Get()
	defer db.Close()
	for sr := range pending {
		res, err := redis.Bytes(findSeatRange.Do(db, sr.Section, sr.Size))
		if err != nil {
			fmt.Println("Reids Nil", err)
		} else {
			err = json.Unmarshal(res, &sr)
			if err != nil {
				fmt.Println("Unmarshal SeatRange Failed", err)
			} else {
				sr.Success = true
				fmt.Println("OK", sr)
			}
		}
		sr.done <- 1
		close(sr.done)
	}
}

// Start the processing goroutine and the web server!
func main() {
	seed()
	go processPending()
	fmt.Println("zseater on port 8080")
	http.HandleFunc("/hello", handleHello)
	http.HandleFunc("/reserve", handleReservation)
	http.HandleFunc("/seed", handleSeed)
	http.HandleFunc("/stats", handleStats)
	http.ListenAndServe(":8080", nil)
}
