# zseater

High performance seat assignment.

* **Fast searching:** uses redis sorted sets to find an acceptable SeatRange in log(n) time.
* **Atomic:** uses redis lua script to provision a SeatRange in an atomic way, eliminating get/set race conditions and the need for locking.
* **Fast and Fair:** uses Go channels to create a FIFO queue for requests. This enables one gouroutine and one redis connection to process many requests.

## Assignment

(verbatim):

A common problem is fans trying to find contiguous seats to attend an event with their friends in the best possible seat within their budget. There are 3 sections with different price points. Historic data shows that inventory for this venue has the following characteristics, 61% of the tickets sold are pairs, the remainder of the tickets are split evenly in groups of 3, 4, 5, 6 contiguous seats.

The seating map attached outlines (X sections, Y rows and Z columns). The system must assign contiguous seats for purchases that are made together. Please leverage at least one algorithm to assign the best available seats in a selected section per the aforementioned ticket distribution, and optimize as best you can to keeping the wasted stets to a minimum, maximizing available inventory and increasing revenue for the company. In your summary if you can compare the algorithms used and which you feel is best suited for this problem in a production state of a near real time system.

(see `stadium.jpg` for stadium layout)

## Design

The core data structure is the **SeatRange** which represents a string of contiguous seats.

The algorithm initializes a venue by creating SeatRange for all the contigious seats in the venue... any isle or split in a string of seats will result in a new SeatRange.

When a request comes in, the algorithm finds a SeatRange of equal or greater length than the party size.  It will prefer ranges that avoid a single remaining seat.

If a range is found the SeatRange is split into two - one for the party and one with the remainder of seats (if not an exact match).  The remainder is returned to the pool for future consideration.

This approach handles fragmentation well.  If a range of size 2 is provisioned but the checkout fails, a range of size 2 can be returned to the pool and becomes eligible for selection.

## Notes

Super basic load testing:

Start server: `go build && ./zseater`

Saturate with requests: `ab -n 10000 -c 200 -r -k http://localhost:8080/reserve`


