package main

import (
	"math/rand"
)

func randomPartySize() int {
	fl := rand.Float32()
	if fl <= 0.61 {
		return 2
	} else {
		return 3 + rand.Intn(4)
	}
}

func randomSection() int {
	return 1 + rand.Intn(3)
}
