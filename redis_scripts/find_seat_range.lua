local sectionName = KEYS[1]
local partySize = ARGV[1]

local findSeatRange = function()
	local found

	-- first try to find an exact fit
	found = redis.call("ZRANGEBYSCORE", sectionName, partySize, partySize, "LIMIT", 0, 1)

	-- next try to find one that avoids dangling seat
	if #found == 0 then
		found = redis.call("ZRANGEBYSCORE", sectionName, partySize + 2, "+inf", "LIMIT", 0, 1)
	end

	-- finally try to find one with a dangling seat
	if #found == 0 then
		found = redis.call("ZRANGEBYSCORE", sectionName, partySize + 1, "+inf", "LIMIT", 0, 1)
	end

	return found[1]
end

local initialRange, sr, remainederRange, result

initialRange = findSeatRange()
if not initialRange then return end

sr = cjson.decode(initialRange)

result = {
	section = sectionName,
	row = sr.row,
	first = sr.first,
	last = sr.first + partySize - 1,
	size = 0 + partySize,
	status = "hold",
}

sr.first = sr.first + partySize
sr.size = sr.size - partySize

redis.call("HINCRBY", "count", "free", -1 * partySize)
redis.call("HINCRBY", "count", "hold", partySize)

redis.call("ZREM", sectionName, initialRange)

if sr.size > 0 then
	remainederRange = cjson.encode(sr)
	redis.call("ZADD", sectionName, sr.size, remainederRange)
end


return cjson.encode(result)

