redis.call("FLUSHDB")

local buildSection = function(sectionNumber, rowCount)
	local key = "Section" .. sectionNumber
	for i=1,rowCount do

		local buildSeatRange = function(first, last)
			local sr = {
				section = sectionNumber,
				row = i,
				first = first,
				last = last,
				size = 1 + last - first,
				status = "free"
			}
			redis.call("ZADD", key, sr.size, cjson.encode(sr))
			redis.call("HINCRBY", "count", "free", sr.size)
		end

		buildSeatRange(1, 25)
		buildSeatRange(26, 35)
		buildSeatRange(36, 75)
	end
end

buildSection(1, 14)
buildSection(2, 17)
buildSection(3, 21)


