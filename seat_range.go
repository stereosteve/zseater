package main

import (
	"fmt"
)

type SeatRange struct {
	Section string `json:"section"`
	Row     int    `json:"row"`
	First   int    `json:"first"`
	Last    int    `json:"last"`
	Size    int    `json:"size"`
	Status  string `json:"status"`

	Success bool `json:"success"`

	done chan int
}

func (sr SeatRange) Len() int {
	return 1 + sr.Last - sr.First
}

func (sr SeatRange) String() string {
	return fmt.Sprintf("%s Row %d Seats %d - %d", sr.Section, sr.Row, sr.First, sr.Last)
}
